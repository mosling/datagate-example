package me.mosling.datagate.example.mqtt;

import me.mosling.datagate.mqtt.MessageClient;
import me.mosling.datagate.mqtt.SampleConsumer;
import me.mosling.httpfixture.common.StringHelper;

public class MainMqttMessage
{
    public static void main( String[] args )
    {
        String amqpServer = StringHelper.nonNullStr( System.getenv( "CLOUDAMQP_URL" ) );

        if ( !amqpServer.isEmpty() )
        {
            MessageClient mc = new MessageClient( "test-service", amqpServer );

            mc.publish( "a.a", "first a message" );
            mc.publish( "a.b", "first b message" );

            new SampleConsumer( mc, "test-channel-a" ).subscribePattern( "*.a", false );
            new SampleConsumer( mc, "test-channel-b" ).subscribePattern( "*.b", true );

            mc.publish( "a.a", "second a message" );
            mc.publish( "a.b", "second b message" );
        }
    }
}

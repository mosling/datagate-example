package me.mosling.datagate.example.mqtt;

import java.util.Random;
import java.util.TimerTask;
import java.util.concurrent.atomic.AtomicInteger;

import me.mosling.datagate.mqtt.MessageClient;

public class PublishRandomNumber
        extends TimerTask
{
    private MessageClient messageClient;
    private AtomicInteger counter = new AtomicInteger( 0 );
    private Random        rand    = new Random();

    @Override
    public void run()
    {
        if ( counter.get() % 2 == 0 )
        {
            messageClient.publish( "random.integer",
                    String.format( "%d -- %d", counter.getAndIncrement(), rand.nextInt() ) );
        }
        else
        {
            messageClient.publish( "random.double",
                    String.format( "%d -- %f", counter.getAndIncrement(), rand.nextDouble() ) );
        }
    }

    public PublishRandomNumber( MessageClient messageClient )
    {
        this.messageClient = messageClient;
    }
}

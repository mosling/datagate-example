package me.mosling.datagate.example.accountmanager;

import java.lang.management.ManagementFactory;
import java.lang.management.MemoryUsage;
import java.util.Date;
import java.util.TimerTask;

import me.mosling.datagate.mqtt.MessageClient;
import me.mosling.httpfixture.common.StringHelper;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class ScheduledExample
        extends TimerTask
{
    private static final Logger LOGGER = LogManager.getLogger(
            ScheduledExample.class );

    private static final Level         always    = Level.getLevel( "ALWAYS" );
    private              boolean       isRunning = false;
    private              String        taskName  = "";
    private              MessageClient messageClient;

    public ScheduledExample( String taskName, MessageClient messageClient )
    {
        super();
        this.taskName = taskName;
        this.messageClient = messageClient;
    }

    @Override
    public void run()
    {
        if ( isRunning )
        {
            LOGGER.error( "restart ScheduledTask '{}'", taskName );
        }
        isRunning = true;
        MemoryUsage m     = ManagementFactory.getMemoryMXBean().getHeapMemoryUsage();
        int         tc    = ManagementFactory.getThreadMXBean().getThreadCount();
        long        toMiB = 1024 * 1024;

        String mcState = "";
        if ( null != messageClient )
        {
            mcState = messageClient.getClientConnection() != null && messageClient.getClientConnection().isOpen() ?
                    " message client connected" :
                    " message client closed";
        }

        LOGGER.log( always, "'{}' triggered at {} --{}; {} threads; memory usage {}/{}MiB", taskName,
                StringHelper.getUtcString( new Date() ), mcState, tc, m.getUsed() / toMiB, m.getMax() / toMiB );
        isRunning = false;
    }

}

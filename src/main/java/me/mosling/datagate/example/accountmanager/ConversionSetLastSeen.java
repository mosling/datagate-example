package me.mosling.datagate.example.accountmanager;

import java.util.Date;

import me.mosling.datagate.conversion.Conversion;
import me.mosling.httpfixture.common.StringHelper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;

public class ConversionSetLastSeen
        implements Conversion
{
    private static final Logger LOGGER = LogManager.getLogger( ConversionSetLastSeen.class );
    private final        String fieldName;

    ConversionSetLastSeen( String fieldName )
    {
        this.fieldName = fieldName;
    }

    @Override
    public String getParameter()
    {
        return "fieldName";
    }

    @Override
    public JSONObject map( JSONObject apiJsonObject )
    {
        if ( !fieldName.isEmpty() )
        {
            if ( apiJsonObject.has( fieldName ) )
            {
                LOGGER.warn( "overwrite field {} contains '{}' with timestamp", fieldName,
                        apiJsonObject.get( fieldName ).toString() );
            }
            apiJsonObject.put( fieldName, StringHelper.getUtcString( new Date() ) );
        }

        return apiJsonObject;
    }

    @Override
    public JSONObject unmap( JSONObject internalJsonObject )
    {
        return internalJsonObject;
    }

    @Override
    public String toString()
    {
        return "(put now() in field '" + fieldName + "')";
    }
}

package me.mosling.datagate.example.accountmanager;

import static spark.Spark.awaitInitialization;
import static spark.Spark.port;

import java.util.*;
import java.util.concurrent.TimeUnit;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import me.mosling.datagate.authentication.*;
import me.mosling.datagate.common.Paths;
import me.mosling.datagate.connector.Connector;
import me.mosling.datagate.connector.ConnectorRest;
import me.mosling.datagate.conversion.*;
import me.mosling.datagate.core.*;
import me.mosling.datagate.fixture.*;
import me.mosling.datagate.formatter.CsvDataFormatter;
import me.mosling.datagate.formatter.DataFormatterDictionary;
import me.mosling.datagate.mqtt.MessageClient;
import me.mosling.datagate.notification.NotificationPublisher;
import me.mosling.datagate.publisher.Publisher;
import me.mosling.datagate.publisher.PublisherVelocity;
import me.mosling.datagate.storage.*;
import me.mosling.httpfixture.common.StringHelper;

public class ApplicationAmRest {
    private static final Logger LOGGER = LogManager.getLogger(
            ApplicationAmRest.class);

    @SuppressWarnings("squid:S3776")
    public static void main(String[] args) {
        // PART 1. setup the application
        // =============================

        // * setup development mode
        Metadata.setDevelopmentMode(!StringHelper.findProperty("development", "").isEmpty());

        // * check if initialization is correct
        if (args.length == 0) {
            LOGGER.error("Please start with a list of application names or resource.");
            return;
        }

        // * set service name
        Metadata.setServiceName(StringHelper.findProperty("service", "datagate-example"));

        // * setup fixture component
        String fixtureName = StringHelper.findProperty("fixture", "");
        String fixtureScopes = StringHelper.findProperty("scopes", "");
        List<String> scopeList = fixtureScopes.isEmpty() ?
                Collections.emptyList() :
                Arrays.asList(fixtureScopes.split(","));
        String httpConnection = StringHelper.findProperty("http", "");
        Fixture fixture = fixtureName.isEmpty() ?
                new FixturePublicHttp("config/fixtures.json", httpConnection) :
                new FixtureAccountManager("config/fixtures.json", fixtureName, StringHelper.findProperty("am_user", ""),
                        StringHelper.findProperty("am_secret", ""), scopeList);

        if (!fixture.isConnected()) {
            LOGGER.error("Please check fixture login parameter (user,secret) .. login failed.");
            LOGGER.info(Metadata.getHeader("OFFLINE mode, fixture not connected"));
        }

        // * application configuration (storage and data manager)
        String databaseUrl = StringHelper.nonNullStr(System.getenv("DATABASE_URL"));
        boolean useMemoryDb = databaseUrl.isEmpty() || !(StringHelper.findProperty("mem-db", "").isEmpty());
        JsonStorage jsonStorage = useMemoryDb ? new JsonStorageMemory() : new JsonStoragePostgres(databaseUrl, "");

        DataManager dataManager = new DataManager(jsonStorage);
        MessageClient amqpClient = null;

        // * create message client and add as notification for datamanager
        String amqpServer = StringHelper.nonNullStr(System.getenv("CLOUDAMQP_URL"));
        if (!amqpServer.isEmpty()) {
            amqpClient = new MessageClient(Metadata.getServiceName(), amqpServer);
            dataManager.setNotification(new NotificationPublisher(amqpClient));
        }

        // * create the rest connector
        Connector rest = new ConnectorRest(dataManager, fixture);

        // * load each configuration from arguments
        for (String arg : args) {
            Metadata.loadConfiguration(arg, rest);
        }

        // * create a conversion to remove the elements _v and _links
        ConversionRemoveFields crf = new ConversionRemoveFields("_v,links", "");
        boolean asReadonly = !StringHelper.findProperty("readonly", "").isEmpty();

        // * modify the resource metadata to adapt to the application and handle the special resource 'role'
        for (Map.Entry<String, ResourceMetadata> r : Metadata.getEntryMapping().entrySet()) {
            String rn = r.getKey();
            ResourceMetadata rmd = r.getValue();

            // ** with a given fixture-name update the storage place
            if (!fixtureName.isEmpty() && System.getProperty("ignoreFixture", "").isEmpty()) {
                rmd.setLocalStorageName(
                        jsonStorage.updateStorageNameWithFixture(rmd.getLocalStorageName(), fixtureName));
            }

            // ** use the same notification for the content merger
            if (rmd.getContentMerger() != null) {
                rmd.getContentMerger().setNotification(dataManager.getNotification());
            }

            // ** add the conversion to each resource with application equal 'am'
            if ("am".equalsIgnoreCase(rmd.getApplication())) {
                ConversionDictionary.add(rmd.getResourceName(), rest.getName(), crf);
            }

            // ** add some special conversions for the resource 'role' and 'user'
            if ("role".equalsIgnoreCase(rn)) {
                List<Conversion> l = new ArrayList<>();
                l.add(new ConversionSetLastSeen("lastSeen"));
                l.add(new ConversionRemoveFields("_v,links",
                        "state,requested,idRequest,targetApplication,responsibleTeam,contactPerson,internalRoleRequest,"
                                + "privilegedRequest,twoFARequest,appDoc,amDoc,lastSeen"));
                ConversionDictionary.add(rmd.getResourceName(), rest.getName(), new ConversionChain(l));
            } else if ("user".equalsIgnoreCase(rn)) {
                List<Conversion> l = new ArrayList<>();
                l.add(new ConversionRemoveFields("_v,links",
                        "externalNames,passwordModificationTimestamp,passwordExpirationTimestamp"));
                l.add(new ConversionTimestampDate("passwordModificationTimestamp,passwordExpirationTimestamp"));
                ConversionDictionary.add(rmd.getResourceName(), rest.getName(),
                        new ConversionChain(l));
            }

            // ** if a memory database is used start to fetch data from remote server
            if (useMemoryDb) {
                LOGGER.info("memory database, prefetch resource data '{}' ...", rn);
                rest.fetchList(rmd);
            }

            // ** each resource ist set to readonly if the property was set
            if (asReadonly) {
                rmd.setRemoteWritable(false);
            }
        }

        // * add authorization and paths (i.e. login, logout) if needed
        String authenticationType = StringHelper.findProperty("auth", "");
        Authentication authentication;

        if ("login".equalsIgnoreCase(authenticationType)) {
            authentication = new AuthenticationAccountManager(fixture, "global-admin");
        } else if ("none".equalsIgnoreCase(authenticationType)) {
            authentication = new AuthenticationNotNeeded(true, "AM_GLOBAL_ADMIN");
        } else {
            authentication = new AuthenticationSimple(jsonStorage, "admin", "Demandware1!");
        }

        // PART 2. setup server and endpoints using sparkjava
        // ==================================================

        // * add the port number
        int portNumber = Integer.parseInt(StringHelper.findProperty("server.port", "4567"));
        port(portNumber);

        // * add static path, this is always needed to access the existing static elements
        SparkConfiguration.addStatic(StringHelper.findProperty("static.path", ""));

        // * enable CORS by calling the method and set some methods and all headers
        SparkConfiguration.enableCORS("GET,POST,PUT,DELETE", "");

        // * add the authentication endpoints from the active authentication
        authentication.addSparkPaths();

        // * create a publisher with the current authentication to have the user roles
        Publisher publisher = new PublisherVelocity(authentication).fixture(fixture);

        // * set optional landing page
        String mainResource = StringHelper.findProperty("mainpage", "");
        String landingPage = Metadata.get(mainResource, false) == null ? "" : Metadata.getPathMapping().get(Paths.WEB_LIST) + mainResource;

        // * add the service specific paths and the experimental chart
        SparkConfiguration.addPaths(landingPage, authentication, dataManager, publisher);
        SparkConfiguration.addChartPath(authentication, publisher);

        // * add data output format CSV for queries with select
        DataFormatterDictionary.add("csv", new CsvDataFormatter(false, ";", "\""));

        // * setup finish by wait for all data (memdb is true) and wait for server startup, set data count for each resource
        dataManager.waitForFetchThreads();
        awaitInitialization();
        Metadata.initializeCountDataFromStorage(jsonStorage);

        // * log some information about the loaded resources
        for (String s : Metadata.getSummary("datagate," + Metadata.getServiceName(), false)) {
            LOGGER.info(s);
        }

        // * add an example scheduled component
        new Timer().schedule(new ScheduledExample("System-Health", amqpClient), 0, TimeUnit.MINUTES.toMillis(1));

        // ready
        LOGGER.info("Server started at port " + portNumber);
        LOGGER.info(Metadata.getHeader("SERVICE READY"));

    }
}
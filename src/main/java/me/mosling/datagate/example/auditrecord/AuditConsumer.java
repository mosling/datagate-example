package me.mosling.datagate.example.auditrecord;

import java.nio.charset.StandardCharsets;

import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Envelope;
import me.mosling.datagate.core.DataManager;
import me.mosling.datagate.core.Metadata;
import me.mosling.datagate.mqtt.MessageClient;
import me.mosling.datagate.mqtt.SampleConsumer;
import org.json.JSONObject;

public class AuditConsumer
        extends SampleConsumer
{
    private DataManager dataManager;

    public AuditConsumer( DataManager dataManager, MessageClient mc, String consumerName )
    {
        super( mc, consumerName );

        this.dataManager = dataManager;
    }

    @Override
    public void handleDelivery( String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body )
    {
        String message = new String( body, StandardCharsets.UTF_8 );

        JSONObject mo = new JSONObject( message );
        JSONObject jo = new JSONObject();

        jo.put( "routing_key", envelope.getRoutingKey() );
        jo.put( "consumer_tag", consumerTag );
        jo.put( "message", mo );

        dataManager.addOrUpdateData( Metadata.get( "audit-entry" ), "", jo );
    }
}

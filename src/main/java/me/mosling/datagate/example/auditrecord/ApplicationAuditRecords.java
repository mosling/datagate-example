package me.mosling.datagate.example.auditrecord;

import static spark.Spark.awaitInitialization;
import static spark.Spark.port;

import me.mosling.datagate.authentication.Authentication;
import me.mosling.datagate.authentication.AuthenticationNotNeeded;
import me.mosling.datagate.common.Paths;
import me.mosling.datagate.core.DataManager;
import me.mosling.datagate.core.Metadata;
import me.mosling.datagate.core.SparkConfiguration;
import me.mosling.datagate.formatter.CsvDataFormatter;
import me.mosling.datagate.formatter.DataFormatterDictionary;
import me.mosling.datagate.mqtt.MessageClient;
import me.mosling.datagate.publisher.Publisher;
import me.mosling.datagate.publisher.PublisherVelocity;
import me.mosling.datagate.storage.JsonStorage;
import me.mosling.datagate.storage.JsonStoragePostgres;
import me.mosling.httpfixture.common.StringHelper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class ApplicationAuditRecords
{
    private static final Logger LOGGER = LogManager.getLogger( ApplicationAuditRecords.class );

    public static void main( String[] args )
    {
        // PART 1 setup the application
        // =============================

        Metadata.setServiceName( "audit-records" );
        JsonStorage storage = new JsonStoragePostgres( StringHelper.nonNullStr( System.getenv( "DATABASE_URL" ) ), "" );

        DataManager dataManager = new DataManager( storage );

        Metadata.loadConfiguration( "audit-records.json", null );

        Authentication authentication = new AuthenticationNotNeeded( true, "" );

        // PART 1.2 adding the mqtt consumer to add incoming audit records to the database
        String amqpServer = StringHelper.nonNullStr( System.getenv( "CLOUDAMQP_URL" ) );
        if ( !amqpServer.isEmpty() )
        {
            MessageClient mc = new MessageClient( "audit-records", amqpServer );

            // * add the consumer for the publisher
            String consumer = "audit-records";
            new AuditConsumer( dataManager, mc, consumer ).subscribeQueue( "am-test" );
            new AuditConsumer( dataManager, mc, consumer ).subscribeQueue( "am-staging" );
            new AuditConsumer( dataManager, mc, consumer ).subscribeQueue( "am-production" );
            new AuditConsumer( dataManager, mc, consumer ).subscribeQueue( "instances" );
        }
        else
        {
            LOGGER.error( "please set the environment variable CLOUADAMQP_URL" );
            return;
        }

        // PART 2 setup server and endpoints using sparkjava
        // ==================================================
        port( Integer.parseInt( StringHelper.findProperty( "server.port", "4567" ) ) );

        SparkConfiguration.addStatic(
                "/Users/steffen.koehler/development/sforce/datagate-example/src/main/resources/public/" );
        SparkConfiguration.enableCORS( "POST,GET,PUT", "Authorization, Content-Type" );

        authentication.addSparkPaths();

        Publisher p = new PublisherVelocity( authentication );

        SparkConfiguration.addPaths( Metadata.getPathMapping().get( Paths.WEB_LIST ) + Metadata.getFirstEntry(),
                authentication, dataManager, p );

        // add data output format CSV for queries with select
        DataFormatterDictionary.add( "csv", new CsvDataFormatter( false, ";", "\"" ) );

        awaitInitialization();

        LOGGER.info( Metadata.getHeader( "AUDIT RECORDS READY" ) );
    }

}

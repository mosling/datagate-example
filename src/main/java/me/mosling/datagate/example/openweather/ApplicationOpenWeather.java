package me.mosling.datagate.example.openweather;

import static spark.Spark.awaitInitialization;
import static spark.Spark.port;

import java.util.Arrays;

import me.mosling.datagate.authentication.Authentication;
import me.mosling.datagate.authentication.AuthenticationNotNeeded;
import me.mosling.datagate.common.Paths;
import me.mosling.datagate.core.DataManager;
import me.mosling.datagate.core.Metadata;
import me.mosling.datagate.core.SparkConfiguration;
import me.mosling.datagate.formatter.CsvDataFormatter;
import me.mosling.datagate.formatter.DataFormatterDictionary;
import me.mosling.datagate.publisher.Publisher;
import me.mosling.datagate.publisher.PublisherVelocity;
import me.mosling.datagate.storage.JsonStorage;
import me.mosling.datagate.storage.JsonStoragePostgres;
import me.mosling.httpfixture.common.StringHelper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class ApplicationOpenWeather
{
    private static final Logger LOGGER = LogManager.getLogger( ApplicationOpenWeather.class );

    public static void main( String[] args )
    {
        // PART 1. setup the application
        // =============================
        Metadata.setServiceName( "open-weather" );
        JsonStorage storage = new JsonStoragePostgres( StringHelper.nonNullStr( System.getenv( "DATABASE_URL" ) ), "" );

        DataManager dataManager = new DataManager( storage );

        Metadata.setMetadataStorage( storage );
        Metadata.loadConfiguration( "open-weather", null );

        new ConnectorOpenWeather( dataManager, Metadata.get( "open-weather" ), "5798d22c6e3468b5eda1ef4c8fa006ed",
                Arrays.asList( "2895044", "2643743", "1689973", "7290588" ) );

        Authentication authentication = new AuthenticationNotNeeded( true, "" );

        // PART 2. setup server and endpoints using sparkjava
        // ==================================================
        String portNumber = StringHelper.findProperty( "server.port", "4567" );
        port( Integer.parseInt( portNumber ) );

        SparkConfiguration.addStatic(
                "/Users/steffen/development/datagate-example/src/main/resources/public/" );
        SparkConfiguration.enableCORS( "POST,GET,PUT", "Authorization, Content-Type" );

        authentication.addSparkPaths();

        Publisher p = new PublisherVelocity( authentication );

        SparkConfiguration.addPaths( Metadata.getPathMapping().get( Paths.WEB_LIST ) + Metadata.getFirstEntry(),
                authentication, dataManager, p );

        // add data output format CSV for queries with select
        DataFormatterDictionary.add( "csv", new CsvDataFormatter( false, ";", "\"" ) );

        awaitInitialization();

        LOGGER.info("Server started at port " + portNumber);
        LOGGER.info( Metadata.getHeader( "OPEN WEATHER READY" ) );
    }
}

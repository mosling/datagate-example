package me.mosling.datagate.example.openweather;

import java.util.Arrays;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;

import me.mosling.datagate.connector.Connector;
import me.mosling.datagate.conversion.*;
import me.mosling.datagate.core.*;
import me.mosling.datagate.fixture.Fixture;
import me.mosling.httpfixture.fixture.*;
import me.mosling.httpfixture.security.JksManagerArgs;

public class ConnectorOpenWeather
        implements Connector {
    private static final Logger LOGGER = LogManager.getLogger(
            ConnectorOpenWeather.class);

    private static final String OWM_KEY = "OPEN_WEATHER_MAP";
    private static final String RMD_NAME = "open-weather";

    private BaseHttpFixture fixture = new BaseHttpFixture();
    private String appId;
    private DataManager dataManager;
    private List<String> cityList;

    public ConnectorOpenWeather(DataManager dataManager, ResourceMetadata rmd, String appId, List<String> cityIdList) {
        this.dataManager = dataManager;
        this.appId = appId;
        cityList = cityIdList;

        fixture.build(new HttpClientArgs().configFile(""), (JksManagerArgs) null);
        fixture.getClient().putHost(OWM_KEY, "http://api.openweathermap.org");

        if (null != rmd) {
            rmd.setConnector(this);

            ConversionTimestampDate ctsd = new ConversionTimestampDate("dt,sys.sunrise,sys.sunset");
            ConversionCreateKey cck = new ConversionCreateKey("dt", "dt,id", "@");
            ConversionDictionary.add(rmd.getResourceName(), this.getName(),
                    new ConversionChain(Arrays.asList(ctsd, cck)));
        }

        LOGGER.info(".. {}", rmd == null ? "failure" : "ready");
    }

    @Override
    public String getName() {
        return "OWM";
    }

    @Override
    public Fixture getFixture() {
        return null;
    }

    @Override
    public DataManager getDataManager() {
        return dataManager;
    }

    @Override
    public String getFixtureName() {
        return "";
    }

    @Override
    public void fetchList(ResourceMetadata rmd) {
        for (String city : cityList) {
            fetchEntry(rmd, city);
        }
    }

    @Override
    public String fetchEntry(ResourceMetadata rmd, String identifier) {
        ResponseData rd = fixture
                .getClient()
                .execute(OWM_KEY, null, Arrays.asList(rmd.getResourcePath(), identifier, appId),
                        HttpClientFactory.defaultRequest("GET", true));

        if (!rmd.getResourceName().equals(RMD_NAME)) {
            rmd = Metadata.get(RMD_NAME);
        }

        if (200 == rd.getStatus() && null != dataManager) {
            dataManager.addOrUpdateData(rmd, getName(), new JSONObject(rd.getResponseContent()));
            return rd.getResponseContent();
        }

        return "";
    }

    @Override
    public void deleteEntry(ResourceMetadata rmd, String entry) {
        // not supported
    }

    @Override
    public String createEntry(ResourceMetadata rmd, String entry, String body) {
        return null;
    }
}

---- select all api clients with a public key
select c.data ->> 'id'                 as API_ID,
       c.data ->> 'name'               as API_NAME,
       c.data ->> 'active'             as API_ACTIVE,
       c.data -> 'organizations' ->> 0 as ORG_ID,
       o.data ->> 'name'               as ORG_NAME
from am_production.am_apiclient c
         left join am_production.am_organization o on o.data ->> 'id' = c.data -> 'organizations' ->> 0
where c.data ->> 'organizations' <> '[]'
  and c.data ->> 'jwtPublicKey' <> ''
order by c.data -> 'organizations' ->> 0;

--- select all user with empty primary organization
select data ->> 'mail',
       data ->> 'userState',
       data ->> 'organizations'
from am_production.am_user
where data ->> 'userState' = 'ENABLED'
  and ((data -> 'primaryOrganization') is null
    or (data ->> 'primaryOrganization') is null);

---- select number of active api clients per organization
with orglist as (select data ->> 'id'   as orgid,
                        data ->> 'name' as orgname
                 from am_production.am_organization),
     apilist as (
         select data ->> 'id'                                                    as apiid,
                data ->> 'name'                                                  as apiname,
                trim(jsonb_array_elements(data -> 'organizations') :: TEXT, '"') as apiorg,
                data ->> 'jwtPublicKey'                                          as pubkey
         from am_production.am_apiclient
         where data ->> 'active' = 'true')
select orgname  as Organization,
       count(*) as "#active API Clients"
from orglist,
     apilist
where apilist.apiorg = orglist.orgid
group by 1
order by 2 desc;

---- select all
select distinct i.data ->> 'realmId'                                             as realm,
                (r.data ->> 'description') || '.' || (r.data ->> 'customerName') as name,
                r.data ->> 'sfAccountId'                                         as sfId,
                o.data ->> 'name'                                                as organization,
                a.data ->> 'name'                                                as client_name,
                a.data ->> 'id'                                                  as client_id,
                a.data ->> 'active'                                              as active
from am_production.am_instance i
         full outer join am_production.am_realm r on (i.data ->> 'realmId' = r.data ->> 'id')
         full outer join am_production.am_organization o on (r.data ->> 'organizationId' = o.data ->> 'id')
         full outer join am_production.am_apiclient a on (a.data -> 'organizations' ? (o.data ->> 'id')::text)
where i.data ->> 'podId' = 'pod80'
  and a.data ->> 'name' notnull
  and r.data ->> 'customerName' = 'sephora';

---- select count of organization types
select data ->> 'type', count(*)
from am_production.am_organization
group by data ->> 'type';

---- select all users with state and primary organization name
select u.data ->> 'mail' as mail, u.data ->> 'userState' as state, o.data ->> 'name' organization
from am_production.am_user u
         left outer join am_production.am_organization o ON (u.data ->> 'primaryOrganization' = o.data ->> 'id');

---- select all role tenant filters
select data ->> 'roleTenantFilterMap'
from am_production.am_user
where data ->> 'roleTenantFilterMap' is not null
  and data ->> 'roleTenantFilterMap' <> '{}';

-- create a role_permission view to later create a matrix between role and permission
create extension tablefunc;

create view role_permission as (select data ->> 'id'                                    as id,
                                       jsonb_array_elements_text(data -> 'permissions') as access,
                                       'X'                                              as marker
                                from am_production.am_role
                                order by 1);

select distinct id
from role_permission;

select *
from crosstab('select access, id, marker from role_permission order by 1,2',
              'select distinct id from role_permission order by 1') as (permission text, account_admin text,
                                                                        api_admin text, api_reader text,
                                                                        global_admin text, ocapi_access_admin text,
                                                                        organization_admin text, realm_info_viewer text,
                                                                        role_admin text);

select array_agg(distinct access || ' text')
from role_permission
order by 1;

drop view role_permission;

---- select count of instances per pod
select data ->> 'pod', count(*)
from inst_srvc_instance
group by data ->> 'pod';

---- select a list with all user/role combinations
select data ->> 'mail', jsonb_array_elements(data -> 'roles')
from am_production.am_user;

---- select count of audit messages by timestamp and routing key
select date(data -> 'message' ->> 'timestamp'), data ->> 'routing_key', count(*)
from audits_audit_entry
group by 1, 2
order by 1;

---- select count of organization per user
with userlist as (select data -> 'roles'                                                    as am_roles,
                         data ->> 'id'                                                      as am_id,
                         data ->> 'mail'                                                    as am_mail,
                         data ->> 'userState'                                               as am_state,
                         (data -> 'organizations') - '11e60cd6-0e24-4874-ae7f-a560f4b45789' as am_orgs -- substract Doc Access Only
                  from am_production.am_user)
select am_id,
       am_mail,
       am_state,
       jsonb_array_length(am_orgs)           as cnt_orgs,
       ((am_roles)::jsonb ? 'global-admin')  as globalAdmin,
       ((am_roles)::jsonb ? 'account-admin') as accountAdmin
from userlist
where (am_mail like '%demandware.com' or am_mail like '%salesforce.com')
  and jsonb_array_length(am_orgs) > 2
order by 4 desc;

---- select all users which exists more than once with the same email
select data ->> 'mail', count(*), array_agg(data ->> 'id')
from am_production.am_user
group by 1
order by 2 desc;

---- select organizations with additional 2FA roles
select data ->> 'id' as Id, data ->> 'name' as Organization, data ->> 'twoFARoles' as "2FA-Roles"
from am_production.am_organization
where jsonb_array_length(data -> 'twoFARoles') > 0;


create view am_production.am_password_modification as
select (data ->> 'passwordModificationTimestamp')::DATE, data ->> 'userState' as state, count(*)
from am_production.am_user
where data ->> 'passwordModificationTimestamp' is not null
group by 1, 2
order by 1;

select created, jsonb_pretty(data) from am_production.am_user  limit 1;